# Prueba de symfony

1. Crear un proyecto en Symfony 4.4 con las dependencias estándar

2. No usar bundles de terceros salvo el de JWT y los demás incluidos por defecto.

3. Instalar JWT

4. Crear un endpoint POST para recibir las credenciales de un usuario y devolver un Token de sesión

5. Devolver el error correspondiente si el login es fallido

6. Crear un endpoint para registrar usuarios en la app

7. Crear un endpoint para listar los usuarios creados, relacionando quien fue su creador

8. Los endpoints de usuarios deben recibir y validar el token de sesión

9. Si el token de sesión es inválido, los endpoints de usuarios deben devolver el error correspondiente.



## Instalacion

Una vez descargado el proyecto es necesario correr en la consola

- composer install
- php bin/console doctrine:database:create (para crear la bd)
- php bin/console doctrine:schema:update --force (para crear tablas y  columnas)

para correr el proyecto es necesario instalar symfony https://symfony.com/download
- luego, en la consola correr: symfony server:start


## Instalacion

El proyecto cuenta con 3 endpoints.

- Crear usuarios:
  - Api : http://127.0.0.1:8000/api/create
  - Method: POST
  - Requerimiento : Token Bearer de autenticacion
  - Json example : {"username": "ember@admin.com","password": "ember"}


- Listar usuarios:
    - Api : http://127.0.0.1:8000/api/list
    - Method : GET
    - Requerimiento : Token Bearer de autenticacion


- Login:
    - Api : http://127.0.0.1:8000/api/login_check
    - Method: POST
    - Requerimiento : Token Bearer de autenticacion
    - Json example : {"username": "ember@admin.com","password": "ember"}


## Nota:
Antes de ejecutar los api es necesario correr este Query en la base de datos, para asi poder generar un usuario de prueba con el cual crees un token y puedas probar correctamente.

INSERT INTO `prueba_syn`.`user` (`id`, `email`, `roles`, `password`, `created_by_id`) VALUES ('1', 'admin@admin.com', '[\"ROLE_ADMIN\"]', '$argon2i$v=19$m=65536,t=4,p=1$SHFXSFhrUDc0QXlYdHc4Yw$UDag8J2b7jDKfIxyhzSIgQooXfPr6V3e5sPvvLC+OH4', '1');

- user: admin@admin.com
- password : admin


