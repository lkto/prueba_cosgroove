<?php

namespace App\Service;

use App\Entity\User;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

class CreateUserService
{
    const VALID_TYPE_EXIST = "El usuario ya se encuentra registrado";
    const VALID_TYPE_FORMAT = "El formato del JSON es incorrecto";

    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @param EntityManagerInterface $entityManager
     * @param UserRepository $userRepository
     * @param UserPasswordEncoderInterface $passwordEncoder
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        UserRepository $userRepository,
        UserPasswordEncoderInterface $passwordEncoder
    )
    {
        $this->passwordEncoder = $passwordEncoder;
        $this->entityManager = $entityManager;
        $this->userRepository = $userRepository;
    }

    public function create(array $user, UserInterface $userCreated): array
    {

        $messaje = '';
        $status = 200;
        $validateUser = $this->ValidateUser($user);
        //var_dump($user, $validateUser);
        if($validateUser['isValid'])
        {

            $NewUser = new User();
            $NewUser->setEmail($user['username']);
            $NewUser->setCreatedBy($userCreated, $userCreated);
            $password = $this->passwordEncoder->encodePassword($NewUser, $user['password']);
            $NewUser->setPassword($password);
            $this->entityManager->persist($NewUser);
            $this->entityManager->flush();
            $messaje = 'El usuario '. $NewUser->getEmail().' Fue creado con exito';
        }
        else
        {
            $messaje = $validateUser["message"];
            $status = 400;
        }

        return [
            'message' => $messaje,
            'status' => $status
        ];
    }

    private function ValidateUser(array $user): array
    {
        $isValid = true;
        $validType = "";


        if($user['username'] && $user['password'])
        {
            $existUser = $this->userRepository->findOneBy(['email'=>$user['username'] ]);
            if($existUser) {
                $isValid = false;
                $validType = self::VALID_TYPE_EXIST;
            }
        }
        else
        {
            $isValid = false;
            $validType = self::VALID_TYPE_FORMAT;
        }

        return [
            'isValid' => $isValid,
            'message' => $validType
        ];
    }

}