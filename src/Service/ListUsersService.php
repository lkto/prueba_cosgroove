<?php

namespace App\Service;

use App\Repository\UserRepository;

class ListUsersService
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }


    public function listUsers(): array
    {
        $usersObject = $this->userRepository->findAll();
        $userArray = [];

        foreach ($usersObject as $user )
        {
            $userTemp = [
                'email' => $user->getEmail(),
                'creador' => $user->getCreatedBy()->getEmail()
            ];

            $userArray[] = $userTemp;
        }

        return $userArray;
    }
}