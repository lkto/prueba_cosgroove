<?php

namespace App\Controller;

use App\Service\CreateUserService;
use App\Service\ListUsersService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @Route("/api")
 */
class ApiController extends AbstractController
{
    /**
     * @Route("/create", name="app_api_create", methods={"POST"})
     * @param Request $request
     * @param CreateUserService $createUserService
     * @return Response
     */
    public function index(Request $request, CreateUserService $createUserService): Response
    {

        $jsonData = json_decode($request->getContent(), true);
        $response = $createUserService->create($jsonData, $this->getUser());
        //dd($this->getUser()->getUsername());
        $data = ['message' => $response];

        return $this->json($data, 200);
    }

    /**
     * @Route("/list", name="app_api_list", methods={"GET"})
     * @param ListUsersService $listUsersService
     * @return Response
     */
    public function list(ListUsersService  $listUsersService): Response
    {
        $users = $listUsersService->listUsers();
        $data = ['users' => $users];
        return $this->json($data, 200);
    }
}
